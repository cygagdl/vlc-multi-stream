
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vlc/vlc.h>
#include <iostream>
#include <string.h>
#include <omp.h>
#include <sys/time.h>
#include <iomanip>
#include <unistd.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>


using namespace cv;
using namespace std;


int VIDEO_WIDTH = 1920;
int VIDEO_HEIGHT = 1080;
//static char * videobuf = 0;
string Vlc_Vertion="";

long long int GetTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

struct ctx
{
    char* name;
    Mat* img;
    uchar* videobuf;
    //HANDLE mutex;
   // uchar* pixels;
};


void *lock(void *data, void**p_pixels)
{
    struct ctx *ctx = (struct ctx*)data;
    *p_pixels = ctx->videobuf;
    return NULL;
}
void display(void *data, void *id)
{
    struct ctx *ctx = (struct ctx*)data;
    imshow(ctx->name,*ctx->img);
    waitKey(10);
    assert(id == NULL);
//    struct ctx *ctx = (struct ctx*)data;
    //ctx->img->data = ctx->videobuf;
//    IplImage *img = cvCreateImage(cvSize(VIDEO_WIDTH, VIDEO_HEIGHT), IPL_DEPTH_8U, 4);
//    img->imageData = videobuf;
    //cvShowImage(libvlc_get_version(), img);
//    cvShowImage(ctx->name, ctx->img);
//    cvWaitKey(10);
//    std::cout<<"show"<<std::endl;
    //cvReleaseImage(&img);
}
void unlock(void *data, void *id, void *const *p_pixels)
{
    (void)data;
    assert(id == NULL);
}

int main()
{
    libvlc_media_t* media1 = NULL;
    libvlc_media_t* media2 = NULL;
    libvlc_media_t* media3 = NULL;
    libvlc_media_player_t* mediaPlayer1 = NULL;
    libvlc_media_player_t* mediaPlayer2 = NULL;
    libvlc_media_player_t* mediaPlayer3 = NULL;
    char const* vlc_args[] =
    {
        "-I",
        "dummy",
        "--ignore-config",
    };
    Vlc_Vertion=libvlc_get_version();
//    videobuf = (char*)malloc((VIDEO_WIDTH * VIDEO_HEIGHT) << 2);
//    memset(videobuf, 0, (VIDEO_WIDTH * VIDEO_HEIGHT) << 2);

    libvlc_instance_t* instance = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);

    //media = libvlc_media_new_path(instance, "/home/zengwei/face/videorace/fashion1.mp4");
    //media = libvlc_media_new_location(instance, "rtsp://192.168.5.11:554/user=admin&password=&channel=1&stream=0.sdp?");
    media1 = libvlc_media_new_location(instance,"rtsp://admin:a1234567@192.168.4.200:554/h264/ch1/main/av_stream");
    mediaPlayer1 = libvlc_media_player_new_from_media(media1);
    libvlc_media_release(media1);
    media2 = libvlc_media_new_location(instance,"rtsp://admin:a1234567@192.168.4.201:554/h264/ch1/main/av_stream");
    mediaPlayer2 = libvlc_media_player_new_from_media(media2);
    libvlc_media_release(media2);
    media3 = libvlc_media_new_location(instance,"rtsp://admin:a1234567@192.168.4.203:554/h264/ch1/main/av_stream");
    mediaPlayer3 = libvlc_media_player_new_from_media(media3);
    libvlc_media_release(media3);

    struct ctx* context1 = (struct ctx*)malloc(sizeof(*context1));
    context1->name = "image1";
    context1->img = new Mat(VIDEO_HEIGHT, VIDEO_WIDTH, CV_8UC3);
    //context1->videobuf = (char*)malloc((VIDEO_WIDTH * VIDEO_HEIGHT) << 2);
    //memset(context1->videobuf, 0, (VIDEO_WIDTH * VIDEO_HEIGHT) << 2);
    context1->videobuf = (unsigned char *)context1->img->data;

    struct ctx* context2 = (struct ctx*)malloc(sizeof(*context2));
    context2->name = "image2";
    context2->img = new Mat(VIDEO_HEIGHT, VIDEO_WIDTH, CV_8UC3);
    //context2->videobuf = (char*)malloc((VIDEO_WIDTH * VIDEO_HEIGHT) << 2);
    //memset(context2->videobuf, 0, (VIDEO_WIDTH * VIDEO_HEIGHT) << 2);
    context2->videobuf = (unsigned char *)context2->img->data;

    struct ctx* context3 = (struct ctx*)malloc(sizeof(*context3));
    context3->name = "image3";
    context3->img = new Mat(720, 1280, CV_8UC3);
    //context3->videobuf = (char*)malloc((1280 * 720) << 2);
    //memset(context3->videobuf, 0, (1280 * 720) << 2);
    context3->videobuf = (unsigned char *)context3->img->data;


    //libvlc_media_player_set_media(mediaPlayer, media);
    libvlc_video_set_callbacks(mediaPlayer1, lock, unlock, display, context1);
    libvlc_video_set_format(mediaPlayer1, "RV24", VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_WIDTH*3);
    libvlc_video_set_callbacks(mediaPlayer2, lock, unlock, display, context2);
    libvlc_video_set_format(mediaPlayer2, "RV24", VIDEO_WIDTH, VIDEO_HEIGHT, VIDEO_WIDTH*3);
    libvlc_video_set_callbacks(mediaPlayer3, lock, unlock, display, context3);
    libvlc_video_set_format(mediaPlayer3, "RV24", 1280, 720, 1280*3);


    libvlc_media_player_play(mediaPlayer1);
    libvlc_media_player_play(mediaPlayer2);
    libvlc_media_player_play(mediaPlayer3);

//        cvShowImage(ctx->name, ctx->img);
//        cvWaitKey(10);
//        std::cout<<"show"<<std::endl;

    int currentFrame=0,frameStop=500;
    Mat frame0, frame1, frame2;
    //stringstream str0,str1,str2;
    bool stop = false;
    long long t;
    while (1)
    {
//       #pragma omp parallel num_threads(3)
//               {
//                  t = GetTime();
//       #pragma omp sections
//                   {
//       #pragma omp section
//                       {
//                        imshow(context1->name,*context1->img);
//                        waitKey(10);
//                        //std::cout<<"cam0 "<<currentFrame<<std::endl;
//                        stringstream str0;
//                        str0<<"/home/cyg/Documents/Qt-Pro/GetImage/cam0/"<< setw(19) << setfill('0') << t*1000000 <<".png";
//                        //imwrite(str0.str(),*context1->img);
//                       }

//       #pragma omp section
//                       {
//                        imshow(context2->name,*context2->img);
//                        waitKey(10);
//                        //std::cout<<"cam1 "<<currentFrame<<std::endl;
//                        stringstream str1;
//                        //cvtColor(frame1,*context2->img,CV_BGR2GRAY);
//                        //cvtColor(frame1,*context2->img,CV_RGB2GRAY);
//                        str1<<"/home/cyg/Documents/Qt-Pro/GetImage/cam1/"<< setw(19) << setfill('0') << t*1000000 <<".png";
//                        //imwrite(str1.str(),*context2->img);
//                       }
//       #pragma omp section
//                       {
//                        imshow(context3->name,*context3->img);
//                        waitKey(10);
//                        //std::cout<<"cam2 "<<currentFrame<<std::endl;
//                        stringstream str2;
//                        //cvtColor(frame2,*context3->img,CV_RGB2GRAY);
//                        str2<<"/home/cyg/Documents/Qt-Pro/GetImage/cam2/"<< setw(19) << setfill('0') << t*1000000 <<".png";
//                        //imwrite(str2.str(),*context3->img);
//                       }
//                   }
//                   //usleep(200000);
//               }
//       #pragma omp barrier
//               currentFrame++;
//               if(currentFrame>=frameStop)
//                   stop=true;
    }
    libvlc_media_player_stop(mediaPlayer1);
    libvlc_media_player_stop(mediaPlayer2);
    libvlc_media_player_stop(mediaPlayer3);
    return 0;
}
