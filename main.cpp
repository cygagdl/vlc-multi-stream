//#include <stdio.h>
#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui.hpp"
#include <vlc/libvlc.h>
#include "vlc/vlc.h"
#include <iostream>
#include <time.h>
#include <unistd.h>

using namespace cv;
#define VIDEO_WIDTH     1920
#define VIDEO_HEIGHT    1080


bool lock = false;
void Lock()
{
    lock = true;
}
void Unlock()
{
    lock = false;
}
bool Islock()
{
    return lock;
}

//struct ctx
//{
//    Mat* image;
//    //HANDLE mutex;
//    uchar* pixels;
//};


void *lock(void *opaque, void **planes)
{
//    struct ctx *ctx = (struct ctx*)data;

//    //WaitForSingleObject(ctx->mutex, INFINITE);

//    // pixel will be stored on image pixel space
//    //put decoded data to ctx->pixels
//    *p_pixels = ctx->pixels;

//    return NULL;


    Lock();
    memset(buffer, 0, sizeof buffer);
    *planes = buffer;
    return NULL;

}

void display(void *opaque, void *picture){
    if (Islock())
       {
           IplImage *img = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 4);
           img->imageData = buffer;
           cvShowImage(name, img);
           cvWaitKey(10);
           cvReleaseImage(&img);
           //fwrite(buffer, sizeof buffer, 1, fp);
       }
}

void unlock(void *opaque, void *picture,  void *const *planes){

//    // get back data structure
//    struct ctx *ctx = (struct ctx*)data;

//    /*static int i = 0;
//    char image[20];
//    sprintf_s(image, "%s%d%s", "..\\image", i++, ".jpg");
//    imwrite(image, *ctx->image);*/
//    std::cout<<"show"<<std::endl;
//    imshow("test", *ctx->image);
//    waitKey(10);
//    //ReleaseMutex(ctx->mutex);

    Unlock();
}



int main(int argc, char* argv[])
{
    libvlc_instance_t * inst;
    libvlc_media_player_t *mp1, *mp2, *mp3;
    libvlc_media_t *m1, *m2, *m3;

    /* Load the VLC engine */
    inst = libvlc_new (0, NULL);

    /* Create a new item */
    m1 = libvlc_media_new_location (inst, "rtsp://admin:a1234567@192.168.4.200:554/h264/ch1/main/av_stream");
    /* Create a media player playing environement */
    mp1 = libvlc_media_player_new_from_media (m1);

    m2 = libvlc_media_new_location (inst, "rtsp://admin:a1234567@192.168.4.201:554/h264/ch1/main/av_stream");
    mp2 = libvlc_media_player_new_from_media (m2);

    m3 = libvlc_media_new_location (inst, "rtsp://admin:a1234567@192.168.4.203:554/h264/ch1/main/av_stream");
    mp3 = libvlc_media_player_new_from_media (m3);


    /* No need to keep the media now */
    libvlc_media_release (m1);
    libvlc_media_release (m2);
    libvlc_media_release (m3);

    //struct ctx* context = (struct ctx*)malloc(sizeof(*context));
    //context->image = new Mat(VIDEO_HEIGHT, VIDEO_WIDTH, CV_8UC3);
    //context->pixels = (unsigned char *)context->image->data;
    libvlc_media_player_set_hwnd(mp1, m1);
    libvlc_video_set_callbacks(mp1, lock, unlock, display, 0);
    libvlc_video_set_format(mp1, "RGBA", VIDEO_WIDTH, VIDEO_HEIGHT,VIDEO_WIDTH*4);


    /* play the media_player */
    libvlc_media_player_play (mp1);

    //libvlc_media_player_play (mp2);

    //libvlc_media_player_play (mp3);


    while(1)
    {
      ;
    }
//    usleep (10000000); /* Let it play a bit */

    /* Stop playing */
    libvlc_media_player_stop (mp1);

    /* Free the media_player */
    libvlc_media_player_release (mp1);

    libvlc_media_player_stop (mp2);

    /* Free the media_player */
    libvlc_media_player_release (mp2);

    libvlc_media_player_stop (mp3);

    /* Free the media_player */
    libvlc_media_player_release (mp3);

    libvlc_release (inst);

    return 0;
}
