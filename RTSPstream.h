#ifndef RTSPSTREAM_H
#define RTSPSTREAM_H

/*
 * Capture RTSP stream in multi-threading fashion
*/
#include "opencv2/opencv.hpp"
#include "queue"
#include <thread>
#include <vlc/vlc.h>
#include "ForegroundDetection/ViBe.h"
#include "IntegralImage.h"
#include <functional>


class StreamDatum{
public:
    StreamDatum(){
        mRGB.create(1,1,CV_8UC3);
        mResizedRGB.create(1,1,CV_8UC3);
    }

    cv::Mat mRGB;
    cv::Mat mResizedRGB;//mResizedForeground;
};

struct StreamCtx
{
    std::string streamID;
    cv::Mat* image;
    uchar* bufPointer;
};

class RTSPstream
{
public:
    RTSPstream(std::string streamUri, float fScale=1.f);
    ~RTSPstream();

    /*returns the first datum of the queue*/
    cv::Ptr<StreamDatum> getStreamDatum();
private:
    bool isRunning;

    struct StreamCtx* ctx;
    static void *lock(void *data, void**p_pixels);
    static void display(void *data, void *id);
    static void unlock(void *data, void *id, void *const *p_pixels);

    //std::queue< cv::Mat> qFrames;
    std::queue< cv::Ptr<StreamDatum> > qData; // this is the buffer containing the data
    cv::Ptr<StreamDatum> returnedDatum;
    cv::VideoCapture stream;
    libvlc_media_t* media = NULL;
    libvlc_media_player_t* mediaPlayer = NULL;
    libvlc_instance_t* instance = NULL;
    int _frame_width, _frame_height;
    float _fScale;

    //BackgroundSubtractor
    //cv::Ptr< bgslibrary::algorithms::IBGS > _pBGsubtractor;

};

#endif // RTSPSTREAM_H
