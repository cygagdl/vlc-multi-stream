#include "RTSPstream.h"
#include <functional>

// thread function for thread processing
void StreamDataThread(StreamCtx* ctx, std::queue< cv::Ptr<StreamDatum> > *qData,
                      float *fScale,
                      bool *isRun)
{
    cv::Ptr< StreamDatum > datum = new StreamDatum();
    while (*isRun){
        if((*qData).size() > 4){
            continue;
        }
        datum->mRGB = *ctx->image;
        (*qData).push(datum);
        //std::cout << "qData.size()=" << (*qData).size() <<std::endl <<std::flush;
    }
}

RTSPstream::RTSPstream(std::string streamUri, float fScale):
_fScale(fScale)
{
    isRunning = false;
    _frame_width =1;
    _frame_height =1;
    //stream.open(streamUri);
    instance = libvlc_new(0, NULL);
    media = libvlc_media_new_location(this->instance,streamUri.c_str());
    mediaPlayer = libvlc_media_player_new_from_media(media);
    libvlc_media_release(media);

    ctx = (struct StreamCtx*)malloc(sizeof(*ctx));
    //ctx->streamID = "";             //camera name
    ctx->image = new cv::Mat(1080, 1920, CV_8UC3);
    ctx->bufPointer = (unsigned char *)ctx->image->data;

    libvlc_video_set_callbacks(mediaPlayer, lock, unlock, display, ctx);
    libvlc_video_set_format(mediaPlayer, "RV24", 1920, 1080, 1920*3);

    libvlc_media_player_play(mediaPlayer);

    //open check
//    if (!stream.isOpened()){
//        std::cerr << "Stream open failed : " << streamUri << std::endl << std::flush;
//        return;
//    }
    isRunning = true;

    //init the return datum
    returnedDatum = new StreamDatum();

    // thread run
    std::thread(StreamDataThread, this->ctx, &this->qData, &this->_fScale, &this->isRunning).detach();

}

RTSPstream::~RTSPstream(){
    isRunning = false;
    delete ctx;
    libvlc_media_player_stop(mediaPlayer);
    delete instance;
}

void *RTSPstream::lock(void *data, void**p_pixels){
    struct StreamCtx *ctx = (struct StreamCtx*)data;
    *p_pixels = ctx->bufPointer;
    return NULL;
}

void RTSPstream::display(void *data, void *id){
    (void)data;
    assert(id == NULL);
}

void RTSPstream::unlock(void *data, void *id, void *const *p_pixels){
    (void)data;
    assert(id == NULL);
}

cv::Ptr<StreamDatum> RTSPstream::getStreamDatum()
{
    //cv::Ptr<StreamDatum> returnedDatum;
    if (!qData.empty()){
        returnedDatum = qData.front();
        // pop only if size > 1, avoid empty queue
        if(qData.size() > 1){
            qData.pop();
        }
    }

    return returnedDatum;
}
