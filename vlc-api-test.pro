#-------------------------------------------------
#
# Project created by QtCreator 2017-09-26T14:50:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = vlc-api-test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += test.cpp


LIBS += /usr/local/lib/*.so \
        /usr/local/vlc/lib/*.so

INCLUDEPATH +=  /usr/local/vlc/include

HEADERS += /usr/local/include/opencv/cv.h \
            /usr/local/include/opencv/cvaux.h \
            /usr/local/include/opencv/highgui.h

