#include "opencv2/opencv.hpp"
#include "../../src/RTSPstream.h"

int main(int, char)
{
    //rtsp address
    std::vector< std::string> streamUris = {
                                          "rtsp://admin:a1234567@192.168.4.200:554/MPEG/ch1/main/av_stream",
                                          "rtsp://admin:a1234567@192.168.4.201:554/MPEG/ch1/main/av_stream"
                                         };

    std::vector<cv::Ptr< RTSPstream >> vCaps;
    for(int i=0;i<streamUris.size();i++)
        vCaps.push_back(new RTSPstream(streamUris[i]));


    while (1){

        for(int i=0;i<vCaps.size();i++){
            cv::Ptr< StreamDatum > datum = vCaps[i]->getStreamDatum();
            if (!datum->mRGB.data)
                continue;
            std::string nameWidget = "cam_"+std::to_string(i);
            cv::imshow(nameWidget,datum->mRGB);
            cv::waitKey(10);

        }
    }

    return 0;
}
